import json
from flask_sqlalchemy import SQLAlchemy
from flask import Flask, request, Response

app = Flask(__name__)
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config["SQLALCHEMY_DATABASE_URI"] ='postgresql://postgres:<password>@localhost/'

db = SQLAlchemy(app)
#define model
class User(db.Model):
    __tablename__ = "users"
    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String(30))
    address = db.Column(db.String(30), nullable=True)
    description = db.Column(db.String(), nullable=True) #for text field

    def __init__(self, name, address, description):
        self.name = name
        self.address = address
        self.description = description

#define routes
@app.route('/users', methods=["POST"])
def create_users():
    try:
        name=request.form["name"]
        address=request.form["address"]
        description = request.form["description"]
        user = User(name=name, address=address, description=description)
        db.session.add(user)
        db.session.commit()
        return Response(
                response=json.dumps({"message":"user created"}),
                status=200,
                mimetype="application/json"
            )
    except Exception as e:
        print(e)
        return Response(
            response = json.dumps({"message":"cannot create users"}),
            status=500,
            mimetype="application/json"
        )

@app.route('/users', methods=["GET"])
def get_users():
    try:
        users = User.query.all()
        all_users = [
            {
                "id":user.id,
                "name": user.name,
                "address": user.address,
                "description": user.description
            } for user in users
        ]
    except Exception as e:
        print(e)
        return Response(
            response = json.dumps({"message":"cannot get users"}),
            status=500,
            mimetype="application/json"
        )

    return json.dumps(all_users)

@app.route('/users/<id>', methods=["GET"])
def find_user(id):
    try:
        user = User.query.get_or_404(id)
        user_obj = {
            "name": user.name,
            "address":user.address,
            "description":user.description
        }
    except Exception as e:
        print(e)
        return Response(
            response = json.dumps({"message":"cannot create users"}),
            status=404,
            mimetype="application/json"
        )
    return json.dumps(user_obj)

@app.route('/users/<id>', methods=["PATCH"])
def update_user(id):
    try:
        user = User.query.get_or_404(id)
        user.name=request.form["name"]
        user.address=request.form["address"]
        user.description = request.form["description"]
        db.session.add(user)
        db.session.commit()
        return Response(
                response=json.dumps({"message":"user updated", "id":f"{id}"}),
                status=200,
                mimetype="application/json"
        )
    except Exception as e:
        print(e)
        return Response(
            response = json.dumps({"message":"unable to update users"}),
            status=404,
            mimetype="application/json"
        )

@app.route('/users/<id>', methods=["DELETE"])
def delete_user(id):
    try:
        user = User.query.get_or_404(id)
        db.session.delete(user)
        db.session.commit()
        return Response(
                response=json.dumps({"message":"user deleted", "id":f"{id}"}),
                status=200,
                mimetype="application/json"
            )
    except Exception as e:
        print(e)
        return Response(
            response = json.dumps({"message":"unable to delete users"}),
            status=404,
            mimetype="application/json"
        )
    


if __name__ == "__main__":
    app.run(debug=True)