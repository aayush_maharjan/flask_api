from flask import Flask, Response, request
import pymongo
import json
from bson.objectid import ObjectId



app = Flask(__name__)
# mongodb+srv://<username>:<password>@sandbox.3dv3m.mongodb.net/myFirstDatabase?retryWrites=true&w=majority
# db = mongo.get_database('hamro_bazar')
# user["expiry_date"] = str(user["expiry_date"])
try:
    mongo = pymongo.MongoClient("mongodb://127.0.0.1:27017")
    db = mongo.facebook
    mongo.server_info()
except Exception as e:
    print(e)
    print("Error while connecting to data base")

@app.route("/users", methods=["GET"])
def get_users():
    try:
        data = list(db.users.find())
        for user in data:
            user["_id"] = str(user["_id"])
        return Response(
            response = json.dumps(data),
            status=200,
            mimetype="application/json"
        )
    except Exception as ex:
        print(ex)
        return Response(
            response = json.dumps({"message":"cannot get users"}),
            status=500,
            mimetype="application/json"
        )


@app.route("/users", methods=["POST"])
def create_users():
    try:
        user = {"name":request.form["name"], 
                "email":request.form["email"], 
                "address":request.form["address"]}
        db_response = db.users.insert_one(user)
        return Response(
            response=json.dumps({"message":"user created", "id":f"{db_response.inserted_id}"}),
            status=200,
            mimetype="application/json"
        )
    except Exception as e:
        print(e)
        return Response(
            response = json.dumps({"message":"cannot create users"}),
            status=500,
            mimetype="application/json"
        )

@app.route("/users/<id>", methods=["PATCH"])
def update_user(id):
    try:
        db_response = db.users.update_one(
            {"_id":ObjectId(id)},
            {"$set":{"name":request.form["name"], "address":request.form["address"], "email":request.form["email"]}}
        )
        if db_response.modified_count == 1:

            return Response(
                response=json.dumps({"message":"user updated"}),
                status=200,
                mimetype="application/json"
            )
        else:
            return Response(
                response=json.dumps({"message":"nothing to updated"}),
                status=200,
                mimetype="application/json"
            )
    except Exception as e:
        print(e)
        return Response(
            response = json.dumps({"message":"cannot update users"}),
            status=500,
            mimetype="application/json"
        )
@app.route("/users/<id>", methods=["DELETE"])
def delete_user(id):
    try:
        db_response = db.users.delete_one({"_id":ObjectId(id)})
        if db_response.deleted_count == 1:
            return Response(
                response=json.dumps({"message":"deleted user", "id":f"{id}"}),
                status=200,
                mimetype="application/json"
            )
        else:
            return Response(
                response=json.dumps({"message":"user not found", "id":f"{id}"}),
                status=200,
                mimetype="application/json"
            )
    except Exception as e:
        print(e)
        return Response(
            response = json.dumps({"message":"cannot delete user"}),
            status=500,
            mimetype="application/json"
        )



if __name__ == "__main__":
    app.run(debug=True)